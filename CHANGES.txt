v0.1 -- 2020-05-09 Initial release.
v0.2 -- 2020-05-10 Updated BufferedPostgresHandler.close() to use CfDatabase.__del__()