# cheesefactory-logging

-----------------

##### A wrapper for Python's logging.
[![PyPI Latest Release](https://img.shields.io/pypi/v/cheesefactory-logging.svg)](https://pypi.org/project/cheesefactory-logging/)
[![PyPI status](https://img.shields.io/pypi/status/cheesefactory-logging.svg)](https://pypi.python.org/pypi/cheesefactory-logging/)
[![PyPI download month](https://img.shields.io/pypi/dm/cheesefactory-logging.svg)](https://pypi.python.org/pypi/cheesefactory-logging/)
[![PyPI download week](https://img.shields.io/pypi/dw/cheesefactory-logging.svg)](https://pypi.python.org/pypi/cheesefactory-logging/)
[![PyPI download day](https://img.shields.io/pypi/dd/cheesefactory-logging.svg)](https://pypi.python.org/pypi/cheesefactory-logging/)

## Main Features

* Built on Python's logging package.
* Includes a handle for buffered logging to a PostgreSQL database.
* Includes a handle for muliprocessing logging.

**Note:** _This package is still in beta status. As such, future versions may not be backwards compatible and features may change._

## Installation
The source is hosted at https://bitbucket.org/hellsgrannies/cheesefactory-logging


```sh
pip install cheesefactory-logging
```

## Dependencies

* Python's built-in logging package.
  
## License

## Parameters



## Examples


