# test_cflogconfigbuilder.py

import time
import glob
import logging.config
import pytest
from cheesefactory_logging import CfLogConfigBuilder

STANDARD_LOG_FORMAT = '%(asctime)s %(levelname)-8s %(filename)-16s %(name)-16s line:%(lineno)-4s ' \
                      '%(process)-5s:%(processName)-19s %(message)s'


def test_level_sanity_check():
    with pytest.raises(ValueError) as excinfo:
        CfLogConfigBuilder.level_sanity_check(log_level='FAIL')
    assert 'Invalid log_level value' in str(excinfo)


def test_sanity_check():
    log_builder = CfLogConfigBuilder(root_log_level='WARNING')
    log_builder.handler_list = ['my_test_handler']
    log_builder.handlers = {'my_test_handler': 'test'}

    with pytest.raises(ValueError) as excinfo:
        log_builder.sanity_check(handler_name='my_test_handler', log_level='DEBUG')
    assert 'Handler already exists: my_test_handler' in str(excinfo)


def test_add_console_handler(capsys):
    log_builder = CfLogConfigBuilder(root_log_level='WARNING')
    log_builder.add_console_handler(log_level='WARNING')

    assert 'console' in log_builder.handler_list, '"console" not in handler_list.'
    assert 'console' in log_builder.handlers, '"console" not in handlers.'
    assert log_builder.get_config()['version'] == 1, 'Bad version section.'
    assert log_builder.get_config()['disable_existing_loggers'] == 0, 'Bad disable_existing_loggers section.'
    assert log_builder.get_config()['root'] == {'level': 'WARNING', 'handlers': ['console']}, 'Bad root section.'
    assert log_builder.get_config()['handlers'] == {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'WARNING',
            'formatter': 'standard',
            'stream': 'ext://sys.stdout'
        }
    }, 'Bad handlers section.'
    assert log_builder.get_config()['formatters'] == {
        'standard': {
            'format': STANDARD_LOG_FORMAT
        }
    }, 'Bad formatters section.'

    logging.config.dictConfig(log_builder.get_config())
    logger = logging.getLogger(__name__)

    logger.debug('You should not see this debug message.')
    logger.warning('You should see this warning message.')
    captured = capsys.readouterr()  # Grab STDOUT and STDERR
    logging.warning(f'\nstdout:\n{captured.out}')
    time.sleep(1)
    assert 'You should not see this debug message' not in captured.out
    assert 'You should see this warning message' in captured.out


def test_add_file_handler():
    filename = 'file_test.log'

    log_builder = CfLogConfigBuilder(root_log_level='WARNING')
    log_builder.add_file_handler(filename=filename, log_level='WARNING')

    assert 'log_file' in log_builder.handler_list, '"log_file" not in handler_list.'
    assert 'log_file' in log_builder.handlers, '"log_file" not in handlers.'
    assert log_builder.get_config()['version'] == 1, 'Bad version section.'
    assert log_builder.get_config()['disable_existing_loggers'] == 0, 'Bad disable_existing_loggers section.'
    assert log_builder.get_config()['root'] == {'level': 'WARNING', 'handlers': ['log_file']}, 'Bad root section.'
    assert log_builder.get_config()['handlers'] == {
        'log_file': {
            'class': 'logging.FileHandler',
            'level': 'WARNING',
            'formatter': 'standard',
            'filename': filename,
            'mode': 'a',
            'encoding': None,
            'delay': False
        }
    }, 'Bad handlers section.'
    assert log_builder.get_config()['formatters'] == {
        'standard': {
            'format': STANDARD_LOG_FORMAT
        }
    }, 'Bad formatters section.'

    logging.config.dictConfig(log_builder.get_config())
    logger = logging.getLogger(__name__)

    logger.debug('You should not see this debug message.')
    logger.warning('You should see this warning message.')

    with open(filename) as fp:
        assert 'You should not see this debug message' not in str(fp.readlines())
        fp.seek(0)
        assert 'You should see this warning message' in str(fp.readlines())


def test_add_rotating_file_handler():
    filename = 'rotating_file_test.log'

    log_builder = CfLogConfigBuilder(root_log_level='WARNING')
    log_builder.add_rotating_file_handler(filename=filename, log_level='WARNING', max_bytes=1024, backup_count=3)

    assert 'rotating_log_file' in log_builder.handler_list, '"rotating_log_file" not in handler_list.'
    assert 'rotating_log_file' in log_builder.handlers, '"rotating_log_file" not in handlers.'
    assert log_builder.get_config()['version'] == 1, 'Bad version section.'
    assert log_builder.get_config()['disable_existing_loggers'] == 0, 'Bad disable_existing_loggers section.'
    assert log_builder.get_config()['root'] == {'level': 'WARNING', 'handlers': ['rotating_log_file']}, \
        'Bad root section.'
    assert log_builder.get_config()['handlers'] == {
        'rotating_log_file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'WARNING',
            'formatter': 'standard',
            'filename': filename,
            'mode': 'a',
            'backupCount': 3,
            'maxBytes': 1024,
            'encoding': None,
            'delay': False
        }
    }, 'Bad handlers section.'
    assert log_builder.get_config()['formatters'] == {
        'standard': {
            'format': STANDARD_LOG_FORMAT
        }
    }, 'Bad formatters section.'

    logging.config.dictConfig(log_builder.get_config())
    logger = logging.getLogger(__name__)

    # Test rotation.
    for x in range(2000):
        if x % 2 == 0:
            logger.debug('You should not see this debug message.')  # Appears in console, but shouldn't in the file.
        else:
            logger.error('You should see this warning message.')

    file_list = glob.glob(f'{filename}*')
    assert len(file_list) == 4, f'Files are not rotating properly. Should have found 3. Found this: {str(file_list)}'

    # Test log level
    for file in file_list:
        logger.debug(file)
        with open(file) as fp:
            assert 'You should not see this debug message' not in str(fp.readlines())
            fp.seek(0)
            assert 'You should see this warning message' in str(fp.readlines())
