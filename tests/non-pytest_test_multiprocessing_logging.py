# non-pytest_test_multiprocessing_logging.py

import tempfile
import glob
import logging.config
import multiprocessing as mp
import cheesefactory_logging

log_directory = tempfile.TemporaryDirectory()
log_file = f'{log_directory.name}/tests.log'

logging.config.dictConfig(
    cheesefactory_logging.logging_settings(
        console=True,
        console_log_level='DEBUG',
        mp_log_file=log_file,
        mp_log_file_level='WARNING',
        mp_log_file_max_size=1 * 1000,
        mp_log_file_max_count=2
    )
)
logger = logging.getLogger(__name__)


def do_something(number):
    """This is what gets spun off into its own process."""
    for y in range(1000):
        if y % 2 == 0:
            logger.debug(f'Pass {number}: You should not see this debug message.')
        else:
            logger.error(f'Pass {number}: You should see this warning message.')


if __name__ == '__main__':

    with mp.Pool() as pool:
        pool.map(do_something, range(24))

    file_list = glob.glob(f'{log_directory.name}/*')
    print(file_list)
    assert len(file_list) == 3, f'Files are not rotating properly. Should have found 3. Found this: {str(file_list)}'

    # Test log level
    for file in file_list:
        logger.debug(file)
        with open(file) as fp:
            assert 'You should not see this debug message' not in str(fp.readlines())
            fp.seek(0)
            assert 'You should see this warning message' in str(fp.readlines())
