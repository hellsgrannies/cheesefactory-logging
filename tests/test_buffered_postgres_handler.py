# test_cfpostgresql.py

import logging
import re
import pytest
from cheesefactory_docker import DockerContainer
from cheesefactory_database.postgresql import CfPostgresql
from cheesefactory_logging.buffered_postgres_handler import BufferedPostgresHandler

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(module)-20s line:%(lineno)-4d %(levelname)-8s %(message)s'
)
logger = logging.getLogger(__name__)

#
# Test config
#
POSTGRES_HOST = '127.0.0.1'
POSTGRES_PORT = '5432/tcp'
POSTGRES_CONTAINER_PORT = 44322
POSTGRES_DATABASE = 'postgres'
POSTGRES_USER = 'postgres'
POSTGRES_PASSWORD = 'ittybittypassword'
POSTGRES_AUTOCOMMIT = True
POSTGRES_DICIONARY_CURSOR = True
POSTGRES_ENCODING = 'utf8'


#
# Container set up and test.
#

@pytest.fixture(scope='module')
def postgres_container():
    """Create a PostgreSQL server Docker container."""

    return DockerContainer(
        container_name='cf_database_test_postgres',
        image='postgres:9.6.17-alpine',
        ports={POSTGRES_PORT: POSTGRES_CONTAINER_PORT},
        detach=True,
        environment=[
            f'POSTGRES_PASSWORD={POSTGRES_PASSWORD}'
        ],
    )


def test_container(postgres_container):
    """Is the container properly running?"""
    logger.debug(f'Container ID: {str(postgres_container)}')
    version = postgres_container.engine_version
    logger.debug(f'version: {version}')
    assert re.search(r'^[0-9]+\.[0-9]+\.[0-9]+$', version) is not None


#
# PostgreSQL connection setup and test.
#

def test_table_not_exists():
    """Establish connection to PostgreSQL server."""
    with pytest.raises(ValueError) as excinfo:
        BufferedPostgresHandler(
            host=POSTGRES_HOST,
            port=str(POSTGRES_CONTAINER_PORT),
            database=POSTGRES_DATABASE,
            user=POSTGRES_USER,
            password=POSTGRES_PASSWORD,
            encoding=POSTGRES_ENCODING,
            table='logs.warehouse'
        )
    assert 'Table does not exist: logs.warehouse' in str(excinfo)


@pytest.fixture(scope='module')
def postgres_server():
    """Establish connection to PostgreSQL server."""
    return CfPostgresql(
        host=POSTGRES_HOST,
        port=str(POSTGRES_CONTAINER_PORT),
        database=POSTGRES_DATABASE,
        user=POSTGRES_USER,
        password=POSTGRES_PASSWORD,
        autocommit=POSTGRES_AUTOCOMMIT,
        dictionary_cursor=POSTGRES_DICIONARY_CURSOR,
        encoding=POSTGRES_ENCODING
    )


def test_postgres(postgres_server):
    """Is the PostgreSQL server running? Tests a non-df SQL execute()"""
    results = postgres_server.execute('SELECT version()')
    logger.debug(f'Find "^PostgreSQL 9.6.17" in results: {str(results[0])}')
    assert re.search('^PostgreSQL 9.6.17', results[0]['version'])


def test_connection_status(postgres_server):
    status = postgres_server.connection_status()
    logger.debug(f'Connection status: {status}')
    assert status == 'OK', f'Status was not OK: {status}'


def test_create_log_table(postgres_server):
    # Sometimes the container doesn't get reset, so perform inserts with conflicts in mind.
    postgres_server.execute(
        'create schema if not exists logs',
        fetchall=False
    )
    postgres_server.execute('''
        create table if not exists logs.warehouse
        (
            id serial not null
                constraint warehouse_pk
                    primary key,
            timestamp timestamp default now(),
            source_host text,
            source_ip text,
            source_app text,
            source_app_args text[],
            event_duration numeric,
            message jsonb
        );
        
        comment on column logs.warehouse.timestamp is 'Time this event was logged.';
        comment on column logs.warehouse.source_host is 'Name of host creating this event.';
        comment on column logs.warehouse.source_ip is 'Source IP of host creating this event.';
        comment on column logs.warehouse.source_app is 'Name of app creating this event.';
        comment on column logs.warehouse.event_duration is 'Duration of event in seconds.';
        comment on column logs.warehouse.message is 'Log details.';
        comment on column logs.warehouse.source_app_args is 'Arguments used when executing source app.';
    ''', fetchall=False)


def test_table_exists(postgres_server):
    table = 'logs.warehouse'
    logger.debug(f'Testing for the existence of table: {table}')
    exist = postgres_server.table_exists(table)
    assert exist is True, f'exist should be True, Instead, it is {exist}'


def test_fields_exist(postgres_server):
    exist = postgres_server.fields_exist(
        'logs.warehouse',
        ['id', 'timestamp', 'source_host', 'source_ip', 'source_app', 'event_duration', 'message',
         'source_app_args']
    )
    assert exist is True, f'exist should be True. Instead, it is {exist}'


@pytest.fixture(scope='module')
def log_server():
    """Establish connection to PostgreSQL server."""
    return BufferedPostgresHandler(
            host=POSTGRES_HOST,
            port=str(POSTGRES_CONTAINER_PORT),
            database=POSTGRES_DATABASE,
            user=POSTGRES_USER,
            password=POSTGRES_PASSWORD,
            encoding=POSTGRES_ENCODING,
            table='logs.warehouse'
        )


def test_log_server(log_server):
    assert log_server.table == 'logs.warehouse', 'log_server.table bad value.'
    assert log_server.duration_start is None, 'log_server.duration_start bad value.'
    assert log_server.record_queue.qsize() == 0, 'log_server.record_queue is bad.'


def test_record_queue(log_server):
    test_logger = logging.getLogger('test_logger')
    test_logger.setLevel(logging.DEBUG)
    db_handler = BufferedPostgresHandler(
        host=POSTGRES_HOST,
        port=str(POSTGRES_CONTAINER_PORT),
        database=POSTGRES_DATABASE,
        user=POSTGRES_USER,
        password=POSTGRES_PASSWORD,
        encoding=POSTGRES_ENCODING,
        table='logs.warehouse'
    )
    db_handler.setLevel(logging.DEBUG)
    test_logger.addHandler(db_handler)

    logger.debug('Inserting logs into queue:')
    test_logger.debug('This is test 1.')
    test_logger.debug('This is test 2.')
    test_logger.debug('This is test 3.')

    assert db_handler.record_queue.qsize() == 3, 'Unexpected queue size.'

    logger.debug('Getting logs from queue:')
    while not db_handler.record_queue.empty():
        record = db_handler.record_queue.get()
        logger.debug(record)
        assert record['filename'] == 'test_buffered_postgres_handler.py', 'Unexpected value for filename'
        assert record['funcName'] == 'test_buffered_postgres_handler.py', 'Unexpected value for funcName'
        assert record['levelname'] == 'DEBUG', 'Unexpected value for levelname'
        assert record['levelno'] == 10, 'Unexpected value for levelno'
        assert record['module'] == 'test_buffered_postgres_handler', 'Unexpected value for module'
        assert 'This is test ' in record['msg'], 'Unexpected value for msg'
        assert record['name'] == 'test_logger', 'Unexpected value for name'


def test_final_emit(log_server, postgres_server):
    test_logger2 = logging.getLogger('test_logger')
    test_logger2.setLevel(logging.DEBUG)
    db_handler = BufferedPostgresHandler(
        host=POSTGRES_HOST,
        port=str(POSTGRES_CONTAINER_PORT),
        database=POSTGRES_DATABASE,
        user=POSTGRES_USER,
        password=POSTGRES_PASSWORD,
        encoding=POSTGRES_ENCODING,
        table='logs.warehouse'
    )
    db_handler.setLevel(logging.DEBUG)
    test_logger2.addHandler(db_handler)

    logger.debug('Inserting logs into queue:')
    test_logger2.debug('This is test 4.')
    test_logger2.debug('This is test 5.')

    assert db_handler.record_queue.qsize() == 2, 'Unexpected queue size.'

    test_logger2.debug('This is test 6.', extra={'end': 1})

    results = postgres_server.execute('select * from logs.warehouse')
    logger.debug(str(results))
