# test_logging_settings.py

import logging
import glob
import multiprocessing as mp
import cheesefactory_logging
import time
import logging.config


def test_console_log_level(capsys):
    logging.config.dictConfig(
        cheesefactory_logging.logging_settings(
            console=True,
            console_log_level='WARNING'
        )
    )
    logger = logging.getLogger(__name__)

    logger.debug('You should not see this debug message.')
    logger.warning('You should see this warning message.')
    captured = capsys.readouterr()  # Grab STDOUT and STDERR
    logging.warning(f'\nstdout:\n{captured.out}')
    time.sleep(3)
    assert 'You should not see this debug message' not in captured.out
    assert 'You should see this warning message' in captured.out


def test_log_file_level(tmpdir):
    log_directory = tmpdir.mkdir('temp_log_file_logs')
    log_file = log_directory.join('test.log')

    logging.config.dictConfig(
        cheesefactory_logging.logging_settings(
            console=True,
            console_log_level='DEBUG',
            log_file=log_file,
            log_file_level='WARNING',
            log_file_max_size=1*1000,
            log_file_max_count=2
        )
    )
    logger = logging.getLogger(__name__)

    # Test rotation.
    for x in range(1000):
        if x % 2 == 0:
            logger.debug('You should not see this debug message.')  # Appears in console, but shouldn't in the file.
        else:
            logger.error('You should see this warning message.')

    file_list = glob.glob(f'{log_directory}/*')
    assert len(file_list) == 3, f'Files are not rotating properly. Should have found 3. Found this: {str(file_list)}'

    # Test log level
    for file in file_list:
        logger.debug(file)
        with open(file) as fp:
            assert 'You should not see this debug message' not in str(fp.readlines())
            fp.seek(0)
            assert 'You should see this warning message' in str(fp.readlines())


def do_something(number):
    """Mapped to a multiprocessing.Pool() by test_mp_log_file()."""
    logger = logging.getLogger(__name__)
    for y in range(1000):
        if y % 2 == 0:
            logger.debug(f'Pass {number}: You should not see this debug message.')
        else:
            logger.error(f'Pass {number}: You should see this warning message.')


def test_mp_log_file(tmpdir):
    log_directory = tmpdir.mkdir('temp_mp_log_file_logs')
    log_file = log_directory.join('tests.log')

    logging.config.dictConfig(
        cheesefactory_logging.logging_settings(
            console=True,
            console_log_level='DEBUG',
            mp_log_file=log_file,
            mp_log_file_level='WARNING',
            mp_log_file_max_size=1 * 1000,
            mp_log_file_max_count=2
        )
    )
    logger = logging.getLogger(__name__)

    with mp.Pool() as pool:
        pool.map(do_something, range(24))

    file_list = glob.glob(f'{log_directory}/*')
    assert len(file_list) == 3, f'Files are not rotating properly. Should have found 3. Found this: {str(file_list)}'

    # Test log level
    for file in file_list:
        logger.debug(file)
        with open(file) as fp:
            assert 'You should not see this debug message' not in str(fp.readlines())
            fp.seek(0)
            assert 'You should see this warning message' in str(fp.readlines())
